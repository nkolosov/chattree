package ChatTree;

import java.util.Date;

/**
 * Created by nkolosov on 17.10.15.
 */
public class Listener {
    protected String name;
    protected Integer port;
    protected Integer lossPercents;

    public void start(String name, Integer port, Integer loss)
    {
        this.name = name;
        this.port = port;
        this.lossPercents = loss;
    }

    public void waitMessage()
    {
        Date time = new Date();
        System.out.println("Current Time; " + time.getTime());
        Date now;

        while(true) {

            now = new Date();
            if ( (now.getTime() - time.getTime()) > 5000) {
                break;
            }
        }
        System.out.println("Current Time; " + now.getTime());
    }
}
