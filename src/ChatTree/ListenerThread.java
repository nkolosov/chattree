package ChatTree;

import java.awt.*;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created by nkolosov on 17.10.15.
 */
public class ListenerThread extends Thread
{
    public static final int MAX_MESSAGE_LENGTH = 2326;
    public static String DELIMITER = ":";

    private int port;

    public ListenerThread(int port)
    {
        this.port = port;
    }

    @Override
    public void run()
    {
        try {
            DatagramSocket ds = new DatagramSocket(this.port);

            while (true) {
                DatagramPacket packet = new DatagramPacket(new byte[MAX_MESSAGE_LENGTH], MAX_MESSAGE_LENGTH);
                ds.receive(packet);

                Message message = this.parseMessage(new String(packet.getData()));
                if (message == null) {
                    continue;
                }

                break;
            }

        } catch (SocketException e) {
            System.err.println("Can not establish connect to port " + this.port);
        } catch (IOException e) {
            System.err.println("IO exception " + e.getMessage());
        }
    }

    protected Message parseMessage(String data)
    {
        String[] parts = data.split(DELIMITER);

        if (parts.length < 2) {
            System.err.println("Invalid message received");
            return null;
        }
        String guid = parts[0];
        MessageType messageType;

        try {
             messageType = MessageType.valueOf(parts[1]);
        } catch (IllegalArgumentException e) {
            System.err.println("Undefined message type received");
            return null;
        }

        if (messageType.compareTo(MessageType.MSG) == 0) {
            if (parts.length != 4) {
                System.out.println("Incorrect argument count for type MSG");
                return null;
            }

            return new Message(guid, messageType, parts[1], parts[2]);
        }

        if (messageType.compareTo(MessageType.PARENT) == 0) {
            if (parts.length != 4) {
                System.out.println("Incorrect argument count for type PARENT");
                return null;
            }

            return new Message(guid, messageType, parts[1], parts[2]);
        }

        return new Message(guid, messageType);
    }
}
