package ChatTree;

/**
 * Created by nkolosov on 17.10.15.
 */
public class Message {
    protected String guid;
    protected MessageType type;
    protected String name;
    protected String message;
    protected String ip;
    protected Integer port;

    public Message(String guid, MessageType type)
    {
        this.guid = guid;
        this.type = type;
    }

    public Message(String guid, MessageType type, String name, String message)
    {
        this.guid = guid;
        this.type = type;
        this.name = name;
        this.message = message;
    }

    public Message(String guid, MessageType type, String ip, Integer port)
    {
        this.guid = guid;
        this.type = type;
        this.ip   = ip;
        this.port = port;
    }

    public String getGuid()
    {
        return guid;
    }

    public MessageType getType()
    {
        return type;
    }

    public String getMessage()
    {
        return message;
    }

    public String getIp()
    {
        return ip;
    }

    public Integer getPort()
    {
        return port;
    }
}
