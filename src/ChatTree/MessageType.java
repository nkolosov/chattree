package ChatTree;

/**
 * Created by nkolosov on 17.10.15.
 */
public enum MessageType
{
    MSG, ACK, PARENT, CHILD, DEAD, BOSS
}
