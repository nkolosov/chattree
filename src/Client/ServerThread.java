package Client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by Compik on 23.09.2014.
 */
public class ServerThread extends Thread{

    private static int bufferSize = 20;
    private LinkedBlockingDeque<String> messageQueue;
    private short port;

    public ServerThread(LinkedBlockingDeque<String> messageQueue, short port) {
        this.messageQueue = messageQueue;
        this.port = port;
    }

    @Override
    public void run() {
        try {
            DatagramSocket dsocket = new DatagramSocket(port);
            byte pData[] = new byte[bufferSize];

            DatagramPacket packet = new DatagramPacket(pData, pData.length);

            while (true) {
                dsocket.receive(packet);

                try {
                    messageQueue.put(new String(packet.getData(), 0, packet.getLength()));
                    messageQueue.put(packet.getAddress().getHostAddress());
                } catch (InterruptedException e) {
                    dsocket.close();
                    Thread.currentThread().interrupt();
                }
            }
        } catch (SocketException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
