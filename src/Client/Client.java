package Client;

import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by Compik on 9/11/2014.
 */
public class Client {
    private static String usage = "Please use: java -jar Client <port> <name>";
    private static String IBORN = "IBORN";
    private static String ILIVE = "ILIVE";
    private static String IDEAD = "IDEAD";

    public static void main(String[] args) {
        if (2 == args.length) {
            try {
                HashMap<String, String> cMachines = new HashMap<String, String>(); // Map of connected machines
                LinkedBlockingDeque<String> messagesQueue = new LinkedBlockingDeque<String>();

                short port = Short.decode(args[0]);
                String hostName = args[1];

                ServerThread serverThread = new ServerThread(messagesQueue, port);
                serverThread.start();

                InetAddress address = InetAddress.getByName("255.255.255.255");

                String lastMessage= IDEAD + hostName;

                DatagramSocket dsocket = new DatagramSocket();
                dsocket.setBroadcast(true);

                ShutdownHook shutdownHook = new ShutdownHook(dsocket, new DatagramPacket(lastMessage.getBytes(), lastMessage.length(), address, port), serverThread);
                Runtime.getRuntime().addShutdownHook(shutdownHook);

                String message= IBORN + hostName;
                String cPort;
                String cAddress;
                String command;

                DatagramPacket Packet = new DatagramPacket(message.getBytes(), message.length(), address, port);

                dsocket.send(Packet);

                while (true) {
                    message = messagesQueue.take();
                    cAddress = messagesQueue.take();
                    command = message.substring(0, 5);

                    if (command.equals(IDEAD)) {
                        cMachines.remove(cAddress, message.substring(5, message.length()));
                    }
                    if (command.equals(ILIVE)) {
                        cMachines.put(cAddress, message.substring(5, message.length()));
                    }
                    if (command.equals(IBORN)) {
                        cMachines.put(cAddress, message.substring(5, message.length()));

                        message = ILIVE + hostName;

                        Packet.setAddress(InetAddress.getByName(cAddress));
                        Packet.setData(message.getBytes());
                        dsocket.send(Packet);
                    }
                    for (int i = 0; i < 50; i++){
                        System.out.println("\n");
                    }
                    System.out.println("IP address\t\tName");
                    Set<Map.Entry<String, String>> pairs = cMachines.entrySet();
                    for(Map.Entry<String, String> p : pairs) {
                        System.out.println(p.getKey() + "\t" + p.getValue());
                    }
                }

            } catch (SocketException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                System.out.println(e.getMessage());
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        } else {
            System.out.println(usage);
            System.exit(-1);
        }
    }
}
