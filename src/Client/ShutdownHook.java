package Client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Created by Compik on 23.09.2014.
 */
public class ShutdownHook extends Thread {

    private DatagramSocket dSocket;
    private DatagramPacket lastPacket;
    private Thread server;

    ShutdownHook(DatagramSocket dSocket, DatagramPacket lastPacket, Thread server){
        this.dSocket = dSocket;
        this.server = server;
        this.lastPacket = lastPacket;
    }

    @Override
    public void run(){
        try {
            dSocket.send(lastPacket);
            dSocket.close();
            server.interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
