package Server;

import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by Compik on 9/11/2014.
 */
public class Server {

    private static String textFileName = "quotes.txt";
    private static String usage = "Please use: java server <port>";
    private static int bufferSize = 4;

    public static void main(String[] args) {
        if (1 == args.length){
            short localPort = Short.decode(args[0]);

            byte[] sData = new byte[bufferSize];
            sData[0] = 'p';
            sData[1] = 'o';
            sData[2] = 'n';
            sData[3] = 'g';

            byte[] cData = new byte[bufferSize];
            cData[0] = 'p';
            cData[1] = 'i';
            cData[2] = 'n';
            cData[3] = 'g';

            try {
                byte[] pData = new byte[bufferSize];
                DatagramPacket Packet = new DatagramPacket(pData, pData.length);

                DatagramSocket dsocket = new DatagramSocket(localPort);
                System.out.println("Server is up and running...");

                while (true){
                    dsocket.receive(Packet);

                    if (Arrays.equals(cData, pData)) {
                        /*Random r = new Random();                      This code emulation server delay
                        int sl = Math.abs(r.nextInt()) / 500000;
                        System.out.println(sl);
                        Thread.currentThread().sleep(sl);*/
                        System.out.println("Receive packet");
                        System.arraycopy(sData, 0, pData, 0, 4);
                        dsocket.send(Packet);
                        System.out.println("Send packet");
                    }
                }
            } catch (SocketException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                System.out.println(e.getMessage());
            } /*catch (InterruptedException e) {
                e.printStackTrace();
            }*/


        }
        else{
            System.out.println(usage);
            System.exit(-1);
        }
    }
}
